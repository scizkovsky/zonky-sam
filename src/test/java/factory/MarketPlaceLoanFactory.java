package factory;

import java.util.ArrayList;
import java.util.List;

import com.scizkovsky.zonky.ukazkabackend.domain.MarketPlaceLoan;

public class MarketPlaceLoanFactory {

	public static MarketPlaceLoan getNewMarketPlaceLoan(int id) {
		MarketPlaceLoan m = new MarketPlaceLoan();
		m.setId(id);
		m.setStory("Story of loan id: " + id);
		return m;
	}

	public static List<MarketPlaceLoan> getNumberOfLoans(int amount) {
		List<MarketPlaceLoan> marketPlaceLoans = new ArrayList<MarketPlaceLoan>();
		for (int i = 0; i < amount; i++) {
			marketPlaceLoans.add(getNewMarketPlaceLoan(i));
		}

		return marketPlaceLoans;
	}

}
