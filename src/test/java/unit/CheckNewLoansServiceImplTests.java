package unit;

import static org.mockito.Mockito.when;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.scizkovsky.zonky.ukazkabackend.domain.MarketPlaceLoan;
import com.scizkovsky.zonky.ukazkabackend.service.CheckNewLoansService;
import com.scizkovsky.zonky.ukazkabackend.service.PrintLoansService;
import com.scizkovsky.zonky.ukazkabackend.service.impl.CheckNewLoansServiceImpl;

import factory.MarketPlaceLoanFactory;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = CheckNewLoansServiceImpl.class)
@TestPropertySource(locations = "classpath:test.properties")
public class CheckNewLoansServiceImplTests {
	@Autowired
	private CheckNewLoansService checkNewLoansService;

	@MockBean
	private RestTemplate restTemplateMock;

	@MockBean
	private PrintLoansService printLoansServiceMock;

	private final static Instant DATE_FROM = Instant.parse("2019-04-12T12:07:16.281Z");
	private final static Instant DATE_TO = Instant.parse("2019-04-12T13:07:16.281Z");
	private final static int NUMBER_OF_PAGES = 10;

	@Before
	public void init() {
		List<MarketPlaceLoan> loans = MarketPlaceLoanFactory.getNumberOfLoans(NUMBER_OF_PAGES);

		ResponseEntity<List<MarketPlaceLoan>> response = new ResponseEntity<List<MarketPlaceLoan>>(loans,
				CollectionUtils.toMultiValueMap(
						Collections.singletonMap("x-total", Arrays.asList(Integer.toString(NUMBER_OF_PAGES)))),
				HttpStatus.OK);

		when(restTemplateMock.exchange(Mockito.anyString(), Mockito.<HttpMethod>eq(HttpMethod.GET),
				Mockito.<HttpEntity<?>>any(), Mockito.<ParameterizedTypeReference<List<MarketPlaceLoan>>>any()))
						.thenReturn(response);

		checkNewLoansService.checkNewLoans(DATE_FROM, DATE_TO);
	}

	@Test
	public void testPrintLoansIsCalled() {

		Mockito.verify(printLoansServiceMock, Mockito.atLeast(1)).printLoans(Mockito.any());

	}

	@Test
	public void testPaging() {

		Mockito.verify(printLoansServiceMock, Mockito.times(NUMBER_OF_PAGES))
				.printLoans(Mockito.<List<MarketPlaceLoan>>any());

	}

	@Test
	public void testExchangeParameters() {

		Mockito.verify(restTemplateMock, Mockito.times(NUMBER_OF_PAGES)).exchange(
				Mockito.contains("https://dummy.test/"), Mockito.<HttpMethod>eq(HttpMethod.GET),
				Mockito.argThat(m -> m.getHeaders().containsKey("X-Order") && m.getHeaders().containsKey("X-Size")
						&& m.getHeaders().containsKey("X-Page")),
				Mockito.<ParameterizedTypeReference<List<MarketPlaceLoan>>>any());

	}

}
