package unit;

import java.time.Instant;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.scizkovsky.zonky.ukazkabackend.scheduled.ScheduledMarketPlace;
import com.scizkovsky.zonky.ukazkabackend.service.CheckNewLoansService;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = ScheduledMarketPlace.class)
public class ScheduledMarketPlaceTests {

	@MockBean
	private CheckNewLoansService checkNewLoansServiceMocked;

	@Autowired
	private ScheduledMarketPlace scheduledMarketPlace;

	@Test
	public void checkNewLoansIsInvoked() {
		scheduledMarketPlace.runMarketPlaceCheck();

		Mockito.verify(checkNewLoansServiceMocked, Mockito.atLeastOnce()).checkNewLoans(Mockito.any(Instant.class),
				Mockito.any(Instant.class));
	}
}
