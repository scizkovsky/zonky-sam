package com.scizkovsky.zonky.ukazkabackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class UkazkabackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(UkazkabackendApplication.class, args);
	}

}
