package com.scizkovsky.zonky.ukazkabackend.scheduled;

import java.time.Instant;
import java.time.ZoneId;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.scizkovsky.zonky.ukazkabackend.service.CheckNewLoansService;

@Service
public class ScheduledMarketPlace {
	private final Logger log = LoggerFactory.getLogger(getClass());

	private Instant timeOfLastRun = null;

	@Value("${check.interval.minutes}")
	private int checkIntervalInMinutes;

	@Autowired
	private CheckNewLoansService checkNewLoansService;

	public ScheduledMarketPlace() {
	}

	/**
	 * Entry point method. Scheduled to run repeatedly and check new loans from
	 * Zonky marketplace.
	 *
	 * @Scheduled - fixed rate equals checkIntervalInMinutes, default 5 minutes =
	 *            300000 ms
	 */
	@Scheduled(fixedRate = 300000)
	public void runMarketPlaceCheck() {
		if (timeOfLastRun == null) {
			timeOfLastRun = Instant.now().minusSeconds(60 * checkIntervalInMinutes);
		}

		Instant currentTime = Instant.now();

		log.info("Scheduled action, checking new loans from time range from: {} to: {}",
				timeOfLastRun.atZone(ZoneId.systemDefault()), currentTime.atZone(ZoneId.systemDefault()));

		checkNewLoansService.checkNewLoans(timeOfLastRun, currentTime);
		timeOfLastRun = currentTime;
	}

}
