package com.scizkovsky.zonky.ukazkabackend.domain;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MarketPlaceLoan {

	private int id;
	private String url;
	private String name;
	private String story;
	private String purpose;
	private List<Photo> photos;
	private int userId;
	private String nickName;
	private int termInMonths;
	private double interestRate;
	private double revenueRate;
	private BigDecimal annuity;
	private BigDecimal premium;
	private String rating;
	private boolean topped;
	private double amount;
	private double remainingInvestment;
	private double investmentRate;
	private boolean covered;
	private double reservedAmount;
	private double zonkyPlusAmount;
	private OffsetDateTime datePublished;
	private boolean published;
	private OffsetDateTime deadline;
	private int investmentsCount;
	private int questionsCount;
	private String region;
	private String mainIncomeType;
	private boolean questionsAllowed;
	private int activeLoansCount;
	private boolean insuranceActive;
	private Collection<InsuranceHistory> insuranceHistory;
	private boolean fastcash;
	private boolean multicash;
	private BigDecimal annuityWithInsurance;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStory() {
		return story;
	}

	public void setStory(String story) {
		this.story = story;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public List<Photo> getPhotos() {
		return photos;
	}

	public void setPhotos(List<Photo> photos) {
		this.photos = photos;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public int getTermInMonths() {
		return termInMonths;
	}

	public void setTermInMonths(int termInMonths) {
		this.termInMonths = termInMonths;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public double getRevenueRate() {
		return revenueRate;
	}

	public void setRevenueRate(double revenueRate) {
		this.revenueRate = revenueRate;
	}

	public BigDecimal getAnnuity() {
		return annuity;
	}

	public void setAnnuity(BigDecimal annuity) {
		this.annuity = annuity;
	}

	public BigDecimal getPremium() {
		return premium;
	}

	public void setPremium(BigDecimal premium) {
		this.premium = premium;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public boolean isTopped() {
		return topped;
	}

	public void setTopped(boolean topped) {
		this.topped = topped;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getRemainingInvestment() {
		return remainingInvestment;
	}

	public void setRemainingInvestment(double remainingInvestment) {
		this.remainingInvestment = remainingInvestment;
	}

	public double getInvestmentRate() {
		return investmentRate;
	}

	public void setInvestmentRate(double investmentRate) {
		this.investmentRate = investmentRate;
	}

	public boolean isCovered() {
		return covered;
	}

	public void setCovered(boolean covered) {
		this.covered = covered;
	}

	public double getReservedAmount() {
		return reservedAmount;
	}

	public void setReservedAmount(double reservedAmount) {
		this.reservedAmount = reservedAmount;
	}

	public double getZonkyPlusAmount() {
		return zonkyPlusAmount;
	}

	public void setZonkyPlusAmount(double zonkyPlusAmount) {
		this.zonkyPlusAmount = zonkyPlusAmount;
	}

	public OffsetDateTime getDatePublished() {
		return datePublished;
	}

	public void setDatePublished(OffsetDateTime datePublished) {
		this.datePublished = datePublished;
	}

	public boolean isPublished() {
		return published;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}

	public OffsetDateTime getDeadline() {
		return deadline;
	}

	public void setDeadline(OffsetDateTime deadline) {
		this.deadline = deadline;
	}

	public int getInvestmentsCount() {
		return investmentsCount;
	}

	public void setInvestmentsCount(int investmentsCount) {
		this.investmentsCount = investmentsCount;
	}

	public int getQuestionsCount() {
		return questionsCount;
	}

	public void setQuestionsCount(int questionsCount) {
		this.questionsCount = questionsCount;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getMainIncomeType() {
		return mainIncomeType;
	}

	public void setMainIncomeType(String mainIncomeType) {
		this.mainIncomeType = mainIncomeType;
	}

	public boolean isQuestionsAllowed() {
		return questionsAllowed;
	}

	public void setQuestionsAllowed(boolean questionsAllowed) {
		this.questionsAllowed = questionsAllowed;
	}

	public int getActiveLoansCount() {
		return activeLoansCount;
	}

	public void setActiveLoansCount(int activeLoansCount) {
		this.activeLoansCount = activeLoansCount;
	}

	public boolean isInsuranceActive() {
		return insuranceActive;
	}

	public void setInsuranceActive(boolean insuranceActive) {
		this.insuranceActive = insuranceActive;
	}

	public Collection<InsuranceHistory> getInsuranceHistory() {
		return insuranceHistory;
	}

	public void setInsuranceHistory(Collection<InsuranceHistory> insuranceHistory) {
		this.insuranceHistory = insuranceHistory;
	}

	public boolean isFastcash() {
		return fastcash;
	}

	public void setFastcash(boolean fastcash) {
		this.fastcash = fastcash;
	}

	public boolean isMulticash() {
		return multicash;
	}

	public void setMulticash(boolean multicash) {
		this.multicash = multicash;
	}

	public BigDecimal getAnnuityWithInsurance() {
		return annuityWithInsurance;
	}

	public void setAnnuityWithInsurance(BigDecimal annuityWithInsurance) {
		this.annuityWithInsurance = annuityWithInsurance;
	}

	@Override
	public String toString() {
		return "MarketPlaceLoan [id=" + id + ", url=" + url + ", name=" + name + ", story=" + story + ", purpose="
				+ purpose + ", photos=" + photos + ", userId=" + userId + ", nickName=" + nickName + ", termInMonths="
				+ termInMonths + ", interestRate=" + interestRate + ", revenueRate=" + revenueRate + ", annuity="
				+ annuity + ", premium=" + premium + ", rating=" + rating + ", topped=" + topped + ", amount=" + amount
				+ ", remainingInvestment=" + remainingInvestment + ", investmentRate=" + investmentRate + ", covered="
				+ covered + ", reservedAmount=" + reservedAmount + ", zonkyPlusAmount=" + zonkyPlusAmount
				+ ", datePublished=" + datePublished + ", published=" + published + ", deadline=" + deadline
				+ ", investmentsCount=" + investmentsCount + ", questionsCount=" + questionsCount + ", region=" + region
				+ ", mainIncomeType=" + mainIncomeType + ", questionsAllowed=" + questionsAllowed
				+ ", activeLoansCount=" + activeLoansCount + ", insuranceActive=" + insuranceActive
				+ ", insuranceHistory=" + insuranceHistory + ", fastcash=" + fastcash + ", multicash=" + multicash
				+ ", annuityWithInsurance=" + annuityWithInsurance + "]";
	}

}
