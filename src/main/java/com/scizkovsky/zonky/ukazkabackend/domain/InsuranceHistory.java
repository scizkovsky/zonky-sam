package com.scizkovsky.zonky.ukazkabackend.domain;

import java.time.LocalDate;

public class InsuranceHistory {

	private LocalDate dateFrom;
	private LocalDate dateTo;

	public LocalDate getDateFrom() {
		return dateFrom;
	}

	public LocalDate getDateTo() {
		return dateTo;
	}

	public void setDateFrom(LocalDate dateFrom) {
		this.dateFrom = dateFrom;
	}

	public void setDateTo(LocalDate dateTo) {
		this.dateTo = dateTo;
	}

	@Override
	public String toString() {
		return "InsuranceHistory [dateFrom=" + dateFrom + ", dateTo=" + dateTo + "]";
	}

}
