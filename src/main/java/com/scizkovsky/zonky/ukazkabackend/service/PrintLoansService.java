package com.scizkovsky.zonky.ukazkabackend.service;

import java.util.List;

import com.scizkovsky.zonky.ukazkabackend.domain.MarketPlaceLoan;

/**
 * Interface to be implemented by any type of print service.
 *
 */
public interface PrintLoansService {

	public void printLoans(List<MarketPlaceLoan> loans);

}
