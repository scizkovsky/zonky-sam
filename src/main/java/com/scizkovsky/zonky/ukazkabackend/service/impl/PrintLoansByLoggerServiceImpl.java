package com.scizkovsky.zonky.ukazkabackend.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.scizkovsky.zonky.ukazkabackend.domain.MarketPlaceLoan;
import com.scizkovsky.zonky.ukazkabackend.service.PrintLoansService;

@Service
public class PrintLoansByLoggerServiceImpl implements PrintLoansService {
	private static final Logger log = LoggerFactory.getLogger(PrintLoansByLoggerServiceImpl.class);

	/**
	 * Prints loans using common toString() method call and slf4j Logger.
	 *
	 * @param loans - List of MarketPlaceLoan to be printed
	 */
	@Override
	public void printLoans(List<MarketPlaceLoan> loans) {

		for (MarketPlaceLoan loan : loans) {
			log.info("-------------------");
			log.info(loan.toString());
		}

	}

}
