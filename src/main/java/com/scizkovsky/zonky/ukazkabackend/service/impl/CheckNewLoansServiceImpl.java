package com.scizkovsky.zonky.ukazkabackend.service.impl;

import java.time.Instant;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.scizkovsky.zonky.ukazkabackend.domain.MarketPlaceLoan;
import com.scizkovsky.zonky.ukazkabackend.service.CheckNewLoansService;
import com.scizkovsky.zonky.ukazkabackend.service.PrintLoansService;

@Service
public class CheckNewLoansServiceImpl implements CheckNewLoansService {
	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private PrintLoansService printLoansService;

	@Value("${check.interval.minutes}")
	private int checkIntervalInMinutes;

	@Value("${zonky.api.url}")
	private String zonkyApiURL;

	@Value("${items.per.page}")
	private int itemsPerPage;

	private static final String LOAN_MARKETPLACE_API = "loans/marketplace";

	@Override
	public void checkNewLoans(Instant datePublishedFromEqual, Instant datePublishedTo) {
		log.info("Checking new loans...");

		ResponseEntity<List<MarketPlaceLoan>> response;
		int pageNumber = 0;
		int totalItems = 0;

		do {
			try {
				response = restTemplate.exchange(getURI(datePublishedFromEqual, datePublishedTo), HttpMethod.GET,
						getEntityWithRequestHeaders(pageNumber),
						new ParameterizedTypeReference<List<MarketPlaceLoan>>() {
						});
			} catch (RestClientException e) {
				log.error("Unfortunately unexpected result occured while connection to remote resouce: {}",
						e.getMessage());
				break;
			}

			totalItems = Integer.parseInt(response.getHeaders().getFirst("x-total"));
			log.info("In specified time range found: {} result(s).", totalItems);

			printLoansService.printLoans(response.getBody());

			pageNumber++;
		} while ((pageNumber * itemsPerPage) < totalItems);

	}

	/**
	 * Prepares request HTTP headers for one of restTemplate.exchange() arguments.
	 *
	 * @param pageNumber - number of page which is the only varible in request HTTP
	 *                   headers.
	 * @return - HTTP headers in proper format of HttpEntity<?>
	 */
	private HttpEntity<String> getEntityWithRequestHeaders(int pageNumber) {
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.set("X-Order", "datePublished");
		requestHeaders.set("X-Size", Integer.toString(itemsPerPage));
		requestHeaders.set("X-Page", Integer.toString(pageNumber));

		return new HttpEntity<>("parameters", requestHeaders);
	}

	/**
	 * Builds proper URI for one of restTemplate.exchange() arguments as URL.
	 *
	 * @param datePublishedFromEqual - earliest time of published loans
	 * @param datePublishedTo        - latest time of published loans
	 * @return - proper URI
	 */
	private final String getURI(Instant datePublishedFromEqual, Instant datePublishedTo) {
		return UriComponentsBuilder.fromUriString(zonkyApiURL + LOAN_MARKETPLACE_API)
				.queryParam("datePublished__gte", datePublishedFromEqual)
				.queryParam("datePublished__lt", datePublishedTo).toUriString();
	}

}
