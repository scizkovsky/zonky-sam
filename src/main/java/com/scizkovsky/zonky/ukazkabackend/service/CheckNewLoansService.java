package com.scizkovsky.zonky.ukazkabackend.service;

import java.time.Instant;

public interface CheckNewLoansService {

	/**
	 *
	 * Prepares proper arguments for restTemplate.exchange() method and calls it,
	 * processes the results and print them using printLoansService.printLoans().
	 *
	 * @param datePublishedFromEqual - earliest time of published loans
	 * @param datePublishedTo        - latest time of published loans
	 */
	public void checkNewLoans(Instant datePublishedFromEqual, Instant datePublishedTo);

}
