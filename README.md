# ukazkabackend
The base of this application was generated using Spring Boot 2.1.4 (SpringInitializr), downloadable at [https://start.spring.io/](https://start.spring.io/).

## Introduction
The aim of this project is to check new loans at zonky.cz every 5 minutes and print the new ones. Using API endpoint [https://api.zonky.cz/loans/marketplace]( https://api.zonky.cz/loans/marketplace).

## Technologies used
* Spring Boot 2.1.4

## Launch
Either by creating and running JAR:

`mvn package && java -jar target/ukazkabackend-0.0.1-SNAPSHOT.jar`

Or by quick compilation:

  `mvn spring-boot:run`

Tests can be run by:

  `mvn test`